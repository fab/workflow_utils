import os
import subprocess
from unittest import mock
import pytest

from workflow_utils import conda


@pytest.fixture(name='conda_base_env', scope='module')
def fixture_conda_base_env(tmpdir_factory):  # pylint: disable=unused-argument
    """
    Yields a path to a tmpdir with a base miniconda env installed.

    See the bin/install-miniconda-env shell script.
    The miniconda installer will be downloaded from repo.anaconda.org.

    Set environment var MINICONDA_INSTALLER_CLEAN=true if you want
    the miniconda installer to be redownloaded each time you run this,
    rather than reusing it to create a new miniconda base env.
    """
    conda_base_env_prefix = str(tmpdir_factory.mktemp('conda_base_env'))

    # Tell install-miniconda-env to install here.
    os.environ['MINICONDA_ENV_PREFIX'] = conda_base_env_prefix

    install_miniconda_env_exec = os.path.join(
        os.path.dirname(os.path.os.path.dirname(__file__)),
        'bin',
        'install-miniconda-env'
    )

    # Only show install-miniconda-env stdout if it fails.
    try:
        subprocess.check_output([install_miniconda_env_exec], text=True, stderr=subprocess.STDOUT)
    except subprocess.CalledProcessError as err:
        print(err.stderr, err.stdout)
        raise err

    # make a clean PATH that doesn't have any other conda execs in it.
    paths = os.environ['PATH'].split(':')
    for path in paths.copy():
        if os.path.exists(os.path.join(path, 'conda')):
            paths.remove(path)

    # Add our new conda base env's bin dir to our path.
    paths = [os.path.join(conda_base_env_prefix, 'bin')] + paths

    # patch PATH in os.environ so that conda_exe can be discovered
    # by find_conda_exe.
    with mock.patch.dict(os.environ, {'PATH': ':'.join(paths)}, clear=True):
        yield conda_base_env_prefix


@pytest.fixture(name='python_project')
def fixture_python_project(tmpdir):
    """
    Yields a path to a fake python project that uses
    pyproject.toml, a conda environment.yml, and a setup.cfg with a minimal pip dependency.
    """
    python_project_path = os.path.join(tmpdir, 'myproject')
    os.makedirs(python_project_path)

    with open(os.path.join(python_project_path, 'environment.yml'), 'wt', encoding='utf-8') as file:
        file.write("""
dependencies:
  - python=3.7
""")

    with open(os.path.join(python_project_path, 'pyproject.toml'), 'wt', encoding='utf-8') as file:
        file.write("""
[build-system]
requires = ["setuptools", "wheel"]
build-backend = "setuptools.build_meta"
""")

    with open(os.path.join(python_project_path, 'setup.cfg'), 'wt', encoding='utf-8') as file:
        file.write("""
[metadata]
name = myproject

[options]
packages = find:
python_requires = >=3.7, <4.0
install_requires =
    yamlreader
""")

    yield python_project_path


def test_find_conda_exe(conda_base_env):
    assert conda.find_conda_exe() == os.path.join(conda_base_env, 'bin', 'conda'), \
        'should find conda in PATH'

    with mock.patch.dict(os.environ, {'CONDA_EXE': '/path/to/env/conda'}):
        assert \
            conda.find_conda_exe(conda_exe='/path/to/custom/conda') == '/path/to/custom/conda', \
            'should find conda via conda_exe param'
        assert conda.find_conda_exe() == '/path/to/env/conda', \
            'should find conda via CONDA_EXE env var'


def test_find_pip_exe(conda_base_env):
    assert conda.find_pip_exe() == os.path.join(conda_base_env, 'bin', 'pip'), \
        'should find pip in PATH'

    with mock.patch.dict(os.environ, {'PIP_EXE': '/path/to/env/pip'}):
        assert \
            conda.find_pip_exe(pip_exe='/path/to/custom/pip') == '/path/to/custom/pip', \
            'should find pip via pip_exe param'
        assert conda.find_pip_exe() == '/path/to/env/pip', \
            'should find conda via PIP_EXE env var'


# pylint: disable=unused-argument
def test_conda_create_and_pack_dist_env(conda_base_env, python_project, tmpdir):
    conda_dist_env_prefix = os.path.join(tmpdir, 'dist', 'conda_dist_env')
    conda_dist_env_dest = os.path.join(tmpdir, 'dist', 'conda_dist_env.tgz')

    conda_packed_env = conda.conda_create_and_pack_dist_env(
        conda_dist_env_prefix,
        conda_dist_env_dest,
        project_path=python_project,
    )

    assert conda_packed_env == conda_dist_env_dest, \
        'Should create and pack conda dist env'

    dist_env_python = os.path.join(conda_dist_env_prefix, 'bin', 'python')
    assert os.path.exists(dist_env_python), \
        'Should have installed python into conda dist env'

    yamlreader_module_path_expected = os.path.join(
        conda_dist_env_prefix, 'lib', 'python3.7', 'site-packages', 'yamlreader', '__init__.py'
    )
    yamlreader_module_path = subprocess.check_output(
        [dist_env_python, '-c', 'import yamlreader; print(yamlreader.__file__)'],
        text=True
    ).strip()
    assert yamlreader_module_path == yamlreader_module_path_expected, \
        "yamlreader should be installed into conda dist env "
