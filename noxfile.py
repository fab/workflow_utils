import nox

@nox.session
def docs(session):
    session.install('.[docs]')
    session.conda_install('make')
    session.cd('docs')
    session.run('sphinx-apidoc', '-f', '-o', './source', '../workflow_utils')
    session.run(
        'make', 'clean', 'html',
        env={'SPHINXBUILD': 'sphinx-build'}
    )
