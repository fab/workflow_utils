# Workflow Utils GitLab CI templates

These templates aim to automate building and releasing artifacts
for job repositories.  Currently, these templates support
conda dist env and python wheel artifacts.  Artifacts are intended to be
published to Gitlab Package Registries.


## Template Layout
- lib: The lib directory contains variables and referencable script snippets only.
  Templates in lib should never declare real CI pipeline jobs.

- jobs: Templates in the jobs directory uses lib templates and declares GitLab CI jobs.

- pipelines: Templates in the pipelines directory compose individual jobs together
  into pipelines for a specific purpose, following specific conventions.

## Project Versioning

The `trigger_release` job (used in the artifact publishing pipelines) assumes that your project
is configured to use [bump2version](https://github.com/c4urself/bump2version),
and adheres to the following development lifecycle (similar to maven release plugin):

- Semantic versioning is used.
- Work is done in development branches and merged into the main branch.
- The main branch is always on a '.dev' release.
- Releases are made by removing the .dev release suffix and committing a tag.

To achieve this, your project should have a .bumpversion.cfg as follows:

```ini
[bumpversion]
current_version = <YOUR_VERSION_HERE>.dev
parse = (?P<major>\d+)\.(?P<minor>\d+)\.(?P<patch>\d+)(\.(?P<release>[a-z0-9]+))?
serialize =
	{major}.{minor}.{patch}.{release}
	{major}.{minor}.{patch}

[bumpversion:part:release]
optional_value = unused
values =
	dev
	unused

# Files in which to bump versions are configured below.
# This can be changed to suit your project.
# This example works with python setup.cfg file.
[bumpversion:file:setup.cfg]
search = version = {current_version}
replace = version = {new_version}
```

### Additional configuration
`trigger_release` requires authentication and read/write permissions to a repository.
Authentication assumes that a valid [project
token](https://docs.gitlab.com/ee/user/project/settings/project_access_tokens.html#project-access-token) has been created and stored in the project's
`CI_PROJECT_PASSWORD` CI/CD variable (**Settings** -> **CI/CD** -> **Variables** -> **Add variable**).
Make sure that `CI_PROJECT_PASSWORD` is masked. This will prevent it from showing in job logs.

A `project_${CI_PROJECT_ID}_bot` user is created by Gitlab upon token generation.

## Example Pipeline Usage

You can use a pipeline by [including](https://docs.gitlab.com/ee/ci/yaml/includes.html)
it in your .gitlab-ci.yml file.

You should still declare and run your test in the `test` stage in your .gitlab-ci.yml
file as usual.

Workflow Utils pipelines expect to work with bump2version as described above,
so they all declare a `trigger_release` job:

A manual `trigger_release` job that can only be run for commits on the main branch.
This will bump to the non .dev version, create a tag, then bump to the next .dev version.
E.g. if current version is 0.1.0.dev, and `POST_RELEASE_VERSION_BUMP` variable is set to
major, then 0.1.0 will be tagged, and the new dev version will be 1.0.0.dev.

The artifact publishing jobs are configured to always run on git tag commits, so by triggering
a release that pushes a git tag, your artifact will automatically be published.


### conda artifact publish pipeline
For use by 'job repositories' that publish conda distribution environments
to a [GitLab Generic Package Registry](https://docs.gitlab.com/ee/user/packages/generic_packages/index.html), defaulting to the current project's.

.gitlab-ci.yml:

```yaml
# Include conda-dist.yml to
# add manual build_conda_env and publish_conda_env jobs.
include:
  - project: 'repos/data-engineering/workflow_utils'
    ref: v0.3.0 # A workflow_utils tag or git ref.
    file: '/gitlab_ci_templates/pipelines/conda_artifact_repo.yml'
```

This will
- Declare the `trigger_release` job as described.
- Declare a `publish_conda_env` job that can be manually run, and is always run for all tag commits.

You can manually run `publish_conda_env` on a development commit to create and publish a downloadable
development snapshot artifact.

Triggering a release will cause a git tag to be created, which will cause the `publish_conda_env`
job to run on the git tag commit's pipeline.


### python lib publish pipeline

Similar to the above, but instead of publishing a conda distribution environment, it
publishes a python wheel to a PyPI Registry, defaulting to the current [GitLab project's
PyPI registry](https://docs.gitlab.com/ee/user/packages/pypi_repository/index.html)

.gitlab-ci.yml:

```yaml
# Include conda-dist.yml to
# add manual build_conda_env and publish_conda_env jobs.
include:
  - project: 'repos/data-engineering/workflow_utils'
    ref: v0.3.0 # A workflow_utils tag or git ref.
    file: '/gitlab_ci_templates/pipelines/python_lib_repo.yml'
```

This will
- Declare the `trigger_release` job as described.
- Declare a `publish_python_wheel` job that can be manually run, and is always run for all tag commits.

You can manually run `publish_python_wheel` on a development commit to create and
publish a downloadable development snapshot artifact.

Triggering a release will cause a git tag to be created, which will cause the `publish_conda_env`
job to run on the git tag commit's pipeline.
